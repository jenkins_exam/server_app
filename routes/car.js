const express = require('express')
const db = require('../db')
const router = express.Router()
const util = require('../utils')

//add car details
router.post('/add', (request, response) => {
  const { companyname, model, price, carColor } = request.body
  const statement = `INSERT INTO carTB (companyname, model, price, carColor)
                     VALUES (?, ?, ?,?)`
  db.pool.query(
    statement,
    [companyname, model, price, carColor],
    (error, cars) => {
      response.send(util.createResult(error, cars))
    }
  )
})

//show all cars
router.get('/show-all', (request, response) => {
  const statement = `SELECT id, companyname, model, price, carColor FROM carTB`
  db.pool.query(statement, (error, cars) => {
    response.send(util.createResult(error, cars))
  })
})

//delete car
router.delete('/delete', (request, response) => {
  const { id } = request.body
  const statement = `DELETE FROM carTB WHERE id=?`
  db.pool.query(statement, [id, request.userId], (error, cars) => {
    response.send(util.createResult(error, cars))
  })
})

module.exports = router
