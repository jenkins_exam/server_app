const express = require('express')
const app = express()
const cors = require('cors')
const utils = require('./utils')
const jwt = require('jsonwebtoken')
const config = require('./config')

app.use(express.json())
app.use(cors())

const userRouter = require('./routes/user')
const carRouter = require('./routes/car')

app.use((request, response, next) => {
  console.log(request.url)
  if (request.url === '/user/signin') {
    next()
  } else {
    const token = request.headers['token']
    if (!token || token.length === 0) {
      response.send(utils.createResult('missing token'))
    } else {
      try {
        const payload = jwt.verify(token, config.secret)
        request.userId = payload.userId
        next()
      } catch (ex) {
        response.send(utils.createResult('invalid token!'))
      }
    }
  }
})

app.use('/user', userRouter)
app.use('/car', carRouter)

app.listen(4000, '0.0.0.0', () => {
  console.log('server started at port 4000')
  process.exit()
})

//D1_36011_Omkar