const mysql = require('mysql')

const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'car_db',
  connectionLimit: 10,
})

module.exports = {
  pool,
}
