const express = require('express')
const router = express.Router()
const db = require('../db')
const jwt = require('jsonwebtoken')
const config = require('../config')

router.post('/signin', (request, response) => {
  const { email, password } = request.body
  const statement = `SELECT empid, name, email FROM userTB
                       WHERE email=? AND password=?`
  db.pool.query(statement, [email, password], (error, users) => {
    const result = {}
    if (error) {
      result['status'] = 'error'
      result['error'] = error
    } else if (users.length === 0) {
      result['status'] = 'error'
      result['error'] = 'user does not exist!'
    } else {
      const user = users[0]
      result['status'] = 'success'
      const token = jwt.sign(
        {
          userId: user['empid'],
        },
        config.secret
      )

      result['data'] = {
        name: user['name'],
        email: user['email'],
        token,
      }
    }
    response.send(result)
  })
})
module.exports = router
